# VIper
A snake clone written in Odin.

VIper uses vim controls (hjkl). Practice the basics while having fun :)

This is my first Odin program, and my first game built with manual memory
management, so it's more of a learning project.

# Build Instructions

VIper has a single dependency: SDL2. On Linux you can install it with your
package manager of choice.

Use the Odin compiler to build the game:

```sh
odin build .
```

Or have it build & run the game with:

```sh
odin run .
```
