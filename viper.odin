package viper

import "core:fmt"
import "game"
import "states"
import sdl "vendor:sdl2"
import ttf "vendor:sdl2/ttf"

main :: proc()
{
	arr1 := [1]int {1}
	arr2 := &arr1
	arr1[0] = 2
	fmt.printf("arr1: %d, arr2: %d", arr1[0], arr2[0])
	init_sdl()
	defer quit()

	states.title_init()
	defer states.title_quit()

	states.game_init()
	defer states.game_quit()

	loop()
}

init_sdl :: proc() {
	fmt.println("Initialising SDL...")
	init_err := sdl.Init({.VIDEO, .TIMER})
	assert(init_err == 0, sdl.GetErrorString())

	game.ctx.window = sdl.CreateWindow(
		"VIper",
		sdl.WINDOWPOS_CENTERED,
		sdl.WINDOWPOS_CENTERED,
		1280,
		720,
		{.SHOWN},
	)

	assert(game.ctx.window != nil, sdl.GetErrorString())
	game.ctx.renderer = sdl.CreateRenderer(game.ctx.window, -1, {.ACCELERATED})
	assert(game.ctx.renderer != nil, sdl.GetErrorString())

	// font
	init_font := ttf.Init()
	assert(init_font == 0, sdl.GetErrorString())
	game.ctx.font = ttf.OpenFont("font/Silver.ttf", 19)
	assert(game.ctx.font != nil, sdl.GetErrorString())

	//game.texts[.Title] = createText("VIper", game.ctx.font, 3)
	//game.texts[.SubTitle] = createText("©2023", game.font)
	//game.texts[.Author] = createText("a game by Dale James", game.font)

	fmt.println("SDL initialised")
}


loop :: proc() {
	event: sdl.Event
	state: states.State

	tick_rate := 60.0
	tick_time := 1000.0 / tick_rate
	dt := 0.0
	game.ctx.time = game.get_time()

	main_loop: for {
		time := game.get_time()
		dt += time - game.ctx.time
		game.ctx.time = time

		for dt >= tick_time {
			state = states.update_state()

			inputptr: states.Input_Callback

			#partial switch state 
			{
			case .Title:
				inputptr = states.title_input

			case .Game:
				inputptr = states.game_input

			case .Gameover:
				inputptr = states.gameover_input
			}

			for sdl.PollEvent(&event) {
				#partial switch event.type 
				{
				case .QUIT:
					break main_loop
				case .KEYDOWN:
					assert(inputptr != nil, "no proc to run :(")
					inputptr(event.key.keysym.scancode)
				}
			}

			#partial switch state 
			{
			case .Title:
				states.title_tick()

			case .Game:
				states.game_tick()

			case .Gameover:
				states.gameover_tick()
			}
			dt -= tick_time
		}

		sdl.SetRenderDrawColor(game.ctx.renderer, 0, 10, 10, 0xff)
		sdl.RenderClear(game.ctx.renderer)

		#partial switch state 
		{
		case .Title:
			states.title_render()

		case .Game:
			states.game_render()

		case .Gameover:
			states.gameover_render()
		}
		//drawText(.Title, 24, 24)
		//drawText(.SubTitle, 24, 83)
		//drawText(.Author, 24, 64)
		sdl.RenderPresent(game.ctx.renderer)
		sdl.Delay(0)
	}
}

quit :: proc() {
	fmt.println("Quitting...")
	ttf.Quit()
	sdl.Quit()
	sdl.DestroyWindow(game.ctx.window)
	sdl.DestroyRenderer(game.ctx.renderer)
	fmt.println("SDL stuff destroyed")
}
