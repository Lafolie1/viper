package game

import sdl "vendor:sdl2"
import ttf "vendor:sdl2/ttf"

COLOR_WHITE : sdl.Color : {255, 255, 255, 255}
COLOR_NONE : sdl.Color : {0, 0, 0, 0}

Game_Context :: struct
{
	window: ^sdl.Window,
	renderer: ^sdl.Renderer,
	font: ^ttf.Font,
	time: f64
}

ctx := Game_Context{}

Text :: struct
{
	tex: ^sdl.Texture,
	dest: sdl.Rect
}

get_time :: proc() -> f64
{
	return f64(sdl.GetPerformanceCounter()) * 1000 / f64(sdl.GetPerformanceFrequency())
}

createText :: proc(
	str: cstring,
	font: ^ttf.Font = ctx.font,
	scale: i32 = 1,
	x: i32 = 0,
	y: i32 = 0) -> Text
{
	surface := ttf.RenderUTF8_Blended_Wrapped(font, str, COLOR_WHITE, 0)
	defer sdl.FreeSurface(surface)

	texture := sdl.CreateTextureFromSurface(ctx.renderer, surface)
	assert(texture != nil, sdl.GetErrorString())
	destRect := sdl.Rect{}
	//ttf.SizeUTF8(font, str, &destRect.w, &destRect.h)
	destRect.w = surface.w * scale
	destRect.h = surface.h * scale
	destRect.x = x
	destRect.y = y
	
	return Text{tex = texture, dest = destRect}
}

renderText :: proc(text: Text)
{
	text := text
	sdl.RenderCopy(ctx.renderer, text.tex, nil, &text.dest)
}

renderText_at_pos :: proc(text: ^Text, x, y: i32)
{
	text.dest.x = x
	text.dest.y = y
	sdl.RenderCopy(ctx.renderer, text.tex, nil, &text.dest)
}

import "core:fmt"
rect_fill :: proc(x, y, w, h: i32)
{
//	fmt.printf("x:%d y:%d w:%d h:%d\n", x, y, w, h)
	sdl.RenderFillRect(ctx.renderer, &sdl.Rect{x = x, y = y, w = w, h = h})
}
