package states

import sdl "vendor:sdl2"
import "core:fmt"
import "../game"

@(private="file")
Title_Text :: enum
{
	Title,
	Author,
	Copyright,
	Controls,
	Hiscore,
	Start,
}

@(private="file")
title_txt : [Title_Text]game.Text

title_init :: proc()
{
	title_txt[.Title] = game.createText(str = "VIper", scale = 3, x = 32, y = 32)
	title_txt[.Author] = game.createText(str = "a game by Dale James", x = 32, y = 72)
	title_txt[.Copyright] = game.createText(str = "© 2023", x = 32, y = 88)
	title_txt[.Controls] = game.createText(str = "Controls:\n hjkl to move\n space to pause\n r to restart\n q to quit", x = 32, y = 140)
	title_txt[.Hiscore] = game.createText(str = "Hiscore:", x = 400, y = 8)
	title_txt[.Start] = game.createText(str = "press space to start", x = 560, y = 320)
}

title_quit :: proc()
{
	for txt in Title_Text
	{
		sdl.DestroyTexture(title_txt[txt].tex)
	}
}

title_enter :: proc()
{

}

title_leave :: proc()
{

}

title_input :: proc(scan: sdl.Scancode)
{
	if scan == sdl.SCANCODE_SPACE
	{
		session.next_state = .Game
	}
}

title_tick :: proc()
{

}

title_render :: proc()
{
	// we want .Start to blink so omit it here
	for txt in Title_Text.Title ..<Title_Text.Start
	{
		game.renderText(title_txt[txt])
	}

	if sdl.GetTicks() % 1000 < 700
	{
		game.renderText(title_txt[.Start])
	}
}
