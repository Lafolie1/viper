package states

import sdl "vendor:sdl2"
import "../game"
import "core:math/rand"
import "core:time"

@private
SNAKE_MAX_LEN :: 80 * 44 - 1

@private
GRID_W :: 80

@private
GRID_H :: 44

@private
Direction :: enum	
{
	Up,
	Down,
	Left,
	Right
}

@private
dir_vec := [Direction][2]i32 {
	.Up = {0, -1},
	.Down = {0, 1},
	.Left = {-1, 0},
	.Right = {1, 0},
}

@private
Snake_Seg :: struct
{
	dir: Direction,
	x, y: i32,
}

@private
Snake :: struct
{
	segs: [SNAKE_MAX_LEN]Snake_Seg,
	head: int,
	tail: int,
	len: int,
	input: Direction,
	offset: i32
}

@private
World :: struct
{
	cells: [GRID_W * GRID_H]bool,
	pill: [2]i32,
	rand: rand.Rand
}

snake: ^Snake
world: ^World

pos2i :: proc(x, y: i32) -> int
{
	return int((y * GRID_W) + x)
}

i2pos :: proc(i: int) -> (i32, i32)
{
	return i32(i % GRID_W), i32(i / GRID_W)
}

move_pill :: proc()
{
	i := rand.int31_max(GRID_W * GRID_H, &world.rand)
	occupied := world.cells[i]
	for occupied
	{
		i = (i + 3) % (GRID_W * GRID_H)
		occupied = world.cells[i]
	}

	world.pill.x, world.pill.y = i2pos(int(i))
}

game_init :: proc()
{
	snake = new(Snake)
	world = new(World)
	rand.init(&world.rand, u64(time.now()._nsec)) 
}

game_quit :: proc()
{
	free(snake)
	free(world)
}

game_enter :: proc()
{
	for i in 0..<GRID_H * GRID_W
	{
		world.cells[i] = false
	}

	for i in 0..=3
	{
		seg := &snake.segs[i]
		seg.x = 16
		seg.y = 16 + 3 - i32(i)
		seg.dir = .Up
		i := pos2i(16, 19 - i32(i))
		world.cells[i] = true
	}
	snake.head = 3
	snake.tail = 0
	snake.len = 4
	snake.input = .Up
	move_pill()
}

game_leave :: proc()
{

}

game_input :: proc(scan: sdl.Scancode)
{
	dir := snake.segs[snake.head].dir

	#partial switch scan
	{
	case .H:
		if dir != .Right
		{
			snake.input = .Left
		}

	case .J:
		if dir != .Up
		{
			snake.input = .Down
		}

	case .K:
		if dir != .Down
		{
			snake.input = .Up
		}

	case .L:
		if dir != .Left
		{
			snake.input = .Right
		}
	}
}

game_tick :: proc()
{
	snake.offset = (snake.offset + 2) % 16

	if snake.offset == 0
	{

		tail := &snake.segs[snake.tail]
		old_head := &snake.segs[snake.head]
		snake.head = (snake.head + 1) % SNAKE_MAX_LEN
		head := &snake.segs[snake.head]


		dir := &dir_vec[old_head.dir]
		head.x = old_head.x + dir.x
		head.y = old_head.y + dir.y
		if head.x < 0
		{
			head.x = GRID_W - 1

		} else if head.x >= GRID_W
		{
			head.x = 0
		}

		if head.y < 0
		{
			head.y = GRID_H - 1

		} else if head.y >= GRID_H
		{
			head.y = 0
		}

		head.dir = snake.input

		collected_pill := head.x == world.pill.x && head.y == world.pill.y
		if !collected_pill
		{
			snake.tail = (snake.tail + 1) % SNAKE_MAX_LEN
			i := pos2i(tail.x, tail.y)
			world.cells[i] = false
		}
		else
		{
			move_pill()
		}

		// check for collision
		i := pos2i(head.x, head.y)
		if world.cells[i]
		{
			session.next_state = .Title
		}

		// occupy the head cell
		world.cells[i] = true
	}
}

render_seg :: proc(seg: Snake_Seg)
{

	dir := &dir_vec[seg.dir]
	offset := dir^ * {snake.offset, snake.offset}
	x := seg.x * 16 + offset.x
	y := seg.y * 16 + offset.y + 16
	game.rect_fill(x, y, 16, 16)

	if seg.x == 0 && seg.dir == .Left
	{
		game.rect_fill(GRID_W * 16 + offset.x, y, 16, 16)	
	}
	else if seg.x == GRID_W - 1 && seg.dir == .Right
	{
		game.rect_fill(-16 + offset.x, y, 16, 16)	
	}
	else if seg.y == 0 && seg.dir == .Up
	{
		game.rect_fill(x, GRID_H * 16 + offset.y + 16, 16, 16)	
	}
	else if seg.y == GRID_H - 1 && seg.dir == .Down
	{
		game.rect_fill(x, -16 + offset.y + 16, 16, 16)	
	}
}

game_render :: proc()
{
	sdl.SetRenderDrawColor(game.ctx.renderer, 180, 180, 130, 255)
	game.rect_fill(world.pill.x * 16, world.pill.y * 16 + 16, 16, 16)

	sdl.SetRenderDrawColor(game.ctx.renderer, 30, 80, 60, 255)
	i := snake.tail
	for i != snake.head
	{
		render_seg(snake.segs[i])
		i = (i + 1) % SNAKE_MAX_LEN
	}

	sdl.SetRenderDrawColor(game.ctx.renderer, 30, 90, 80, 255)
	render_seg(snake.segs[snake.head])

	sdl.SetRenderDrawColor(game.ctx.renderer, 10, 20, 30, 255)
	game.rect_fill(0, 0, 1280, 16)


	//sdl.SetRenderDrawColor(game.ctx.renderer, 200, 20, 20, 255)
	//for occ, k in world.cells
	//{
	//	if occ
	//	{
	//		x, y := i2pos(k)
	//		game.rect_fill(x * 16, y * 16 + 16, 16, 16)
	//	}
	//}
}
