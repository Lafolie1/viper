package states

import sdl "vendor:sdl2"

Input_Callback :: proc(scan: sdl.Scancode)

State :: enum
{
	None,
	Title,
	Game,
	Gameover,
}

@(private)
Session :: struct
{
	next_state: State,
	state:      State,
	score:      u64,
	hiscore:    u64,
}

@(private)
session := Session{next_state = .Title}

update_state :: proc() -> State
{
	next := session.next_state
	if next == .None && session.state != .None
	{
		return session.state
	}

	#partial switch session.state 
	{
	case .Title:
		title_leave()
	case .Game:
		game_leave()
	case .Gameover:
		gameover_leave()
	}

	// fallback to .Title
	next = next == nil ? .Title : next
	#partial switch next 
	{
	case .Title:
		title_enter()
	case .Game:
		game_enter()
	case .Gameover:
		gameover_enter()
	}

	session.state = next
	session.next_state = .None
	return session.state
}
