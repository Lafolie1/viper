package states

import sdl "vendor:sdl2"
import "core:fmt"

gameover_enter :: proc()
{

}

gameover_leave :: proc()
{

}

gameover_input :: proc(scan: sdl.Scancode)
{
	fmt.printf("%s", scan)
}

gameover_tick :: proc()
{

}

gameover_render :: proc()
{

}
